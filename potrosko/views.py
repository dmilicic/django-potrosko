import json
import logging

from django.http import HttpResponse
from django.http import Http404

from models import *
from hack import startscript

# dictionary keys
NUMBER = "Number"
OPERATOR = "Operator"

def get_numbers(request):

	if request.method == "GET":
		raise Http404()

	# all the requested numbers from a POST request
	requested_numbers = []
	# leftover numbers that are not stored in the database
	leftover_numbers = []
	# array containing the response with number-operator information
	response_array = []
	# result array from hack script execution
	hackscript_result = []

	# load all the requested phone numbers to an array
	for number in request.POST:
		requested_numbers.append(request.POST[number])

 	# get all the available operators from the database
	all_operators = Operator.objects.all()
	logging.info("Operator number: " + str(len(all_operators)))
	operator_map = dict()
	for operator in all_operators:
		operator_map[operator.name] = operator
		logging.info(operator.name)

	# all_numbers = PhoneNumber.objects.all()
	# for num in all_numbers:
	# 	print num.number

	# check for each number if it is already in the database
	for number in requested_numbers:

		if number[:3] != "385":
			logging.info("Skipping... " + number)
			continue

		try:
			entry = PhoneNumber.objects.get(number = number)
			numberOperator = dict()
			numberOperator[NUMBER] = entry.number
			numberOperator[OPERATOR] = entry.operator.name
			
			if numberOperator[OPERATOR] != "":
				response_array.append(numberOperator)
		except PhoneNumber.DoesNotExist:
			leftover_numbers.append(number)

	logging.info("Leftover... " + str(leftover_numbers))

	# call the hack script to retrieve number-operator information from HAKOM
	if leftover_numbers:
		hackscript_result = startscript(leftover_numbers)

	# add all new numbers to database
	for numberOperator in hackscript_result:

		# check if retrieved operator equals any operator from the database
		try:
			operator = operator_map[numberOperator[OPERATOR]]
			entry = PhoneNumber(operator = operator, number = numberOperator[NUMBER])
			entry.save()

			# put the new numbers in the response array
			if numberOperator[OPERATOR] != "":
				response_array.append(numberOperator)
		except:
			logging.error("No such operator... [" + numberOperator[OPERATOR] + "] " + "[" + numberOperator[NUMBER] + "] ")

	json_dump = json.dumps(response_array, sort_keys=True, indent=4, separators=(',', ': '))
	return HttpResponse(json_dump, content_type="application/json")