from django.db import models

class Operator(models.Model):
	name = models.CharField(max_length=30, blank=True)

class PhoneNumber(models.Model):
    operator = models.ForeignKey(Operator)
    number = models.CharField(max_length=20)
    date = models.DateTimeField(auto_now_add=True)
