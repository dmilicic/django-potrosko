import urllib
import urllib2 
import cookielib
import md5
import json
import os

VERBATIM = False
JSON = False

FILENAME = os.path.join(os.path.dirname(__file__), "numbers.txt")
FILE_OUT = os.path.join(os.path.dirname(__file__), "numbers.out")

HAKOM = "http://www.hakom.hr/default.aspx?id=62"
VALID_IMG = "http://www.hakom.hr/validImg.aspx"
VALIDATION = "id='_validacija'"
VALIDATION_NAME = "_validacija"
VALUE = "value='"
OPERATOR = "Operator :</th><td>"


BLACK = "93c335bbbee10c726246ac3eba15f91e"
BLUE = "29804ebfd3927e16497b24cb3372b2fd"
GREEN = "a5a685be294015927bfd2bf6a40f7b4c"
YELLOW = "0c70e2e6c5a829a87029ce4e6e4307da"
ORANGE = "069539366500d824d74640d4ef23afbb"


html = ''
values = {'sto' : 'prijenosBroja',
		'brojTel' : '385989064230' 
      }
validation_array = []



def fetch(uri):

	if VERBATIM:
		print "Fetching url: ", uri
	
	req = urllib2.Request(uri)
	return opener.open(req)

def post(uri):

	if VERBATIM:
		print "Requesting phonenumber info..."

	data = urllib.urlencode(values)
	req = urllib2.Request(uri)
	return opener.open(req, data)

# def add_cookie():
# 	for cookie in cookies:
# 		print cookie.name, cookie.value
# 		if(cookie.name == 'ASP.NET_SessionId'):
# 			# opener.addheaders.append(('Cookie', 'ASP.NET_SessionId=' + cookie.value))
# 			print "Adding cookies: ", cookie.name, cookie.value
# 	print '\n'


def getValues(html):
	arr = []
	validationIdx = 0
	for i in range(5):
		validationIdx = html.find(VALIDATION, validationIdx + 1)
		idx =  html.find(VALUE, validationIdx)

		valueIdx = idx + len(VALUE)
		value = html[valueIdx:html.find("'", valueIdx)]
		arr.append(value)
	return arr

def getOperator():
	operatorIdx = html.find(OPERATOR)
	l = operatorIdx + len(OPERATOR)

	if VERBATIM:
		print html[l:html.find("<", l)]
	return html[l:html.find("<", l)]


def getImgHash():	
	arr = validation_array

	img = fetch(VALID_IMG)
	md.update(img.read())
	digest = md.hexdigest()

	if(digest == BLACK):
		values[VALIDATION_NAME] = arr[0]

	elif(digest == BLUE):
		values[VALIDATION_NAME] = arr[1]

	elif(digest == GREEN):
		values[VALIDATION_NAME] = arr[2]

	elif(digest == YELLOW):
		values[VALIDATION_NAME] = arr[3]

	elif(digest == ORANGE):
		values[VALIDATION_NAME] = arr[4]

	else:
		print "FAIL", digest


def getNumbers():
	f = open(FILENAME, "r")
	numbers = [line.strip() for line in f]
	return numbers


def main(numbers):
	result = []
	for number in numbers:
		values['brojTel'] = number

		res = fetch(HAKOM)

		global md
		md = md5.new()

		global html
		html = res.read()
		
		global validation_array
		validation_array = getValues(html)

		getImgHash()
		html = post(HAKOM).read()

		operators = dict()
		operators['Number'] = number
		operators['Operator'] = getOperator()
		result.append(operators)
	
	dump = ""
	if JSON:
		dump = json.dumps(result, sort_keys=True, indent=4, separators=(',', ': '))
	else:
		dump = result

	if VERBATIM:
		print dump

	return dump

def startscript(numbers, json = False, verbatim = False):

	# init global values
	global JSON
	JSON = json

	global VERBATIM
	VERBATIM = verbatim

	md = md5.new()

	cookies = cookielib.LWPCookieJar()

	handlers = [
		urllib2.HTTPHandler(),
		urllib2.HTTPSHandler(),
		urllib2.HTTPCookieProcessor(cookies)
		# urllib2.ProxyHandler({'http': '212.200.246.202'})
	]

	global opener
	opener = urllib2.build_opener(*handlers)

	# start main script
	return main(numbers)

if __name__ == "__main__":
	startscript(getNumbers())