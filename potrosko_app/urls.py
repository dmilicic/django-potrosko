from django.conf.urls import patterns, url, include
from django.views.generic import TemplateView

handler500 = 'djangotoolbox.errorviews.server_error'

urlpatterns = patterns('',
	('^tariffs$', 'potrosko_app.views.get_tariffs'),
	('^numbers$', 'potrosko_app.views.get_numbers'),
    ('^$', TemplateView.as_view(template_name="index.html"),
     {'template': 'index.html'}),
)
