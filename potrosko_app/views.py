import json
import logging

from django.http import HttpResponse
from django.http import Http404

from models import *
from hack import startscript

import os.path


MODULE_DIR = os.path.dirname(__file__)  # get current directory
# tariff filename
TARIFF_FILE = os.path.join(MODULE_DIR, 'tariffs.txt')

# dictionary keys
NUMBER = "Number"
OPERATOR = "Operator"

def get_tariffs(request):

	if request.method == "POST":
		raise Http404()

	tariffs = open(TARIFF_FILE, "r")
	data = json.load(tariffs)
	json_dump = json.dumps(data, sort_keys=True, indent=4, separators=(',', ': '))
	return HttpResponse(json_dump, content_type="application/json")


def get_numbers(request):

	if request.method == "GET":
		raise Http404()

	# all the requested numbers from a POST request
	requested_numbers = []
	# leftover numbers that are not stored in the database
	leftover_numbers = []
	# array containing the response with number-operator information
	response_array = []
	# result array from hack script execution
	hackscript_result = []

	# load all the requested phone numbers to an array
	for number in request.POST:
		requested_numbers.append(request.POST[number])

 	# get all the available operators from the database
	all_operators = Operator.objects.all()

	# check the number of stored number-operator information
	number_count = len(PhoneNumber.objects.all())
	logging.info("Phonenumbers in db: " + str(number_count))

	# check for each number if it is already in the database
	for number in requested_numbers:

		if number[:3] != "385":
			logging.info("Skipping... " + number)
			continue

		try:
			entry = PhoneNumber.objects.get(number = number)
			numberOperator = dict()
			numberOperator[NUMBER] = entry.number
			numberOperator[OPERATOR] = entry.operator.name

			if len(numberOperator[OPERATOR]) == 0:
				continue

			response_array.append(numberOperator)
		except PhoneNumber.DoesNotExist:
			leftover_numbers.append(number)

	# call the hack script to retrieve number-operator information from HAKOM
	if leftover_numbers:
		hackscript_result = startscript(leftover_numbers)

	# add all new numbers to database
	for numberOperator in hackscript_result:
		# check if retrieved operator equals any operator from the database
		for operator in all_operators:

			if len(operator.name) == 0:
				continue

			if(operator.name == numberOperator[OPERATOR]):
				entry = PhoneNumber(operator = operator, number = numberOperator[NUMBER])
				entry.save()

		# put the new numbers in the response array
		response_array.append(numberOperator)	

	json_dump = json.dumps(response_array, sort_keys=True, indent=4, separators=(',', ': '))
	return HttpResponse(json_dump, content_type="application/json")